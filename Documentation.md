# Odeeo

## Integration Steps

1) **"Install"** or **"Upload"** FG Odeeo plugin from the FunGames Integration Manager in Unity, or download it here.

2) Follow the instructions in the **"Install External Plugin"** section to import PlayOn SDK.

3) Click on the **"Prefabs and Settings"** button in the FunGames Integration Manager to fill up your scene with required components and create the Settings asset.

4) To finish your integration, follow the **Account and Settings** section.

## Install External Plugin

After importing the FG Odeeo module from the Integration Manager window, you will find the last compatible version of PlayOn SDK in the _Assets > FunGames_Externals > AudioAds_ folder. Double click on the .unitypackage file to install it.

**Note that the versions of included external SDKs are the latest tested and approved by our development team. We recommend using these versions to avoid any type of discrepencies. If, for any reason, you already have a different version of the plugin installed in your project that you wish to keep, please advise our dev team.**

## Account and Settings

Ask your Publisher to create your app on the Odeeo account and to provide you the latest Unity Package and credentials for your app.

In the FG Odeeo Settings (_Assets > Resources > FunGames > FGOdeeoSettings_) you will have to fill:

- the App Key for Android if your app is intended to be published in the GooglePlay Store
- The App Key for IOS & the App Store ID if your app is intended to be published in the App Store
- The color of the progress bar displayed when playing audios
- The position of the banner (for ads with no Logos)
- The button options for skippable ads
- **The Ad Unit Placements you are using in the game. (make sure to only enable the Ad Format you are using for your game)**

In order to change the position/size of Logo placements, you can modify it in the PlayOnAdAnchor game object inside the FGOdeeo prefab.
